import Sidebar from "./components/sidebar/Sidebar";
import Topbar from "./components/topBar/Topbar";
import './components/app.css'
import Home from "./pages/home/Home";
import UserList from "./pages/userList/UserList";
import User from "./pages/user/User";
import NewUser from "./pages/newUser/NewUser";
import ProductList from "./pages/productList/ProductList";
import Product from "./pages/product/Product";
import NewProduct from "./pages/newProduct/NewProduct";
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";



function App() {
  return (
    <Router>
      <Topbar/>
      <div className="container">
        <Sidebar/>
        <Routes>
          <Route exact path="/" element={<Home />}> </Route>
          <Route path="/users" element={<UserList />}> </Route>
          <Route path="/user/:userID" element={<User />}> </Route>
          <Route path="/newUser" element={<NewUser />}> </Route>
          <Route path="/products" element={<ProductList />}> </Route>
          <Route path="/product/:productID" element={<Product />}> </Route>
          <Route path="/newProduct" element={<NewProduct />}> </Route>
        </Routes>
      </div>
    </Router>
  )
}

export default App;
