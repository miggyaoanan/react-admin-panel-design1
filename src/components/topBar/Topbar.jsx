import React from 'react'
import './topbar.css'

import { NotificationsNone, Language, Settings } from '@material-ui/icons';

export default function Topbar() {
  return (
    <div className='topbar'>
        <div className="topbarWrapper">
            <div className="topLeft">
                <span className="logo">Miggy Admin</span>
            </div>
            <div className="topRight">
                <div className="topbarIconContainer">
                    <NotificationsNone/>
                    <span className="topIconBadge">2</span>
                </div>
                <div className="topbarIconContainer">
                    <Language/>
                    <span className="topIconBadge">2</span>
                </div>
                <div className="topbarIconContainer">
                    <Settings/>                  
                </div>
                <img src="https://images.pexels.com/photos/7595188/pexels-photo-7595188.jpeg?auto=compress&cs=tinysrgb&w=1600" alt="" className="topAvatar" />
            </div>
        </div>
    </div>
  )
}
