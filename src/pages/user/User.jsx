import { PermIdentity, 
        Today, 
        PhoneAndroid, 
        MailOutline ,
        Place,
        Publish
    } from '@material-ui/icons'
import { Link } from 'react-router-dom'

import './user.css'

const User = () => {
  return (
    <div className='user'>
        <div className="userTitleContainer">
            <h1 className='userTitle'>Edit User</h1>
            <Link to="/newUser">
                <button className="userAddButton">Create</button>
            </Link>
        </div>
        <div className="userContainer">
            <div className="userShow">
                <div className="userShowTop">
                    <img src="https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="" className="userShowImg" />
                    <div className="userShowTopTitle">
                        <span className="userShowUserName">Miggy Erquiza</span>
                        <span className="userShowUserTitle">Software Engineer</span>
                    </div>
                </div>
                <div className="userShowBottom">
                    <span className="userShowTitle">Account Details</span>
                    <div className="userShowInfo">
                        <PermIdentity className='userShowIcon'/>
                        <span className="showInfoTitle">miggy2022</span>
                    </div>
                    <div className="userShowInfo">
                        <Today className='userShowIcon'/>
                        <span className="showInfoTitle">03 16 1987</span>
                    </div>
                    <span className="userShowTitle">Contact Details</span>
                    <div className="userShowInfo">
                        <PhoneAndroid className='userShowIcon'/>
                        <span className="showInfoTitle">999 3380 913</span>
                    </div>
                    <div className="userShowInfo">
                        <MailOutline className='userShowIcon'/>
                        <span className="showInfoTitle">miggy2022@gmail.com</span>
                    </div>
                    <div className="userShowInfo">
                        <Place className='userShowIcon'/>
                        <span className="showInfoTitle">Makati City</span>
                    </div>
                </div>
            </div>
            <div className="userUpdate">
                <span className="userUpdateTitle">Edit</span>
                <form  className="userUpdateForm">
                    <div className="userUpdateLeft">
                        <div className="userUpdateItem">
                            <label>Username</label>
                            <input type="text" 
                                placeholder='miggy2022' 
                                className="userUpdateInput" 
                            />
                        </div>
                        <div className="userUpdateItem">
                            <label>Full Name</label>
                            <input type="text" 
                                placeholder='Miggy Erquiza' 
                                className="userUpdateInput" 
                            />
                        </div>
                        <div className="userUpdateItem">
                            <label>Email</label>
                            <input type="text" 
                                placeholder='miggy2022@gmail.com' 
                                className="userUpdateInput" 
                            />
                        </div>
                        <div className="userUpdateItem">
                            <label>Phone</label>
                            <input type="text" 
                                placeholder='999 3380 913' 
                                className="userUpdateInput" 
                            />
                        </div>
                        <div className="userUpdateItem">
                            <label>Address</label>
                            <input type="text" 
                                placeholder='Makati City' 
                                className="userUpdateInput" 
                            />
                        </div>
                        
                    </div>
                    <div className="userUpdateRight">
                        <div className="userUpdateUpload">
                            <img className='userUpdateImg' src="https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="" />

                            <label htmlFor="file"> <Publish className='userUpdateIcon' /> </label>
                            <input type="file" id='file' style={{display:"none"}}/>
                        </div>
                        <button className="userUpdateButton">Update</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
  )
}

export default User