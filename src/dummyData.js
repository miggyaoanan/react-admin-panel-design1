export const userData = [
  {
    name: "Jan",
    "Active User": 4000,
  },
  {
    name: "Feb",
    "Active User": 3000,
  },
  {
    name: "Mar",
    "Active User": 5000,
  },
  {
    name: "Apr",
    "Active User": 4000,
  },
  {
    name: "May",
    "Active User": 3000,
  },
  {
    name: "Jun",
    "Active User": 2000,
  },
  {
    name: "Jul",
    "Active User": 4000,
  },
  {
    name: "Agu",
    "Active User": 3000,
  },
  {
    name: "Sep",
    "Active User": 4000,
  },
  {
    name: "Oct",
    "Active User": 1000,
  },
  {
    name: "Nov",
    "Active User": 4000,
  },
  {
    name: "Dec",
    "Active User": 3000,
  },
];

export const productData = [
  {
    name: "Jan",
    "Sales": 4000,
  },
  {
    name: "Feb",
    "Sales": 3000,
  },
  {
    name: "Mar",
    "Sales": 5000,
  },
 
];

export const userRows = [

  { id: 1, username: 'Jon Snow', avatar: 'https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  email: 'jon@gmail.com',
  status: 'Active',
  transaction: 'PHP 100,000',
   },
  { id: 2, username: 'Jon Snow', avatar: 'https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  email: 'jon@gmail.com',
  status: 'Active',
  transaction: 'PHP 100,000',
   },
  { id: 3, username: 'Jon Snow', avatar: 'https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  email: 'jon@gmail.com',
  status: 'Active',
  transaction: 'PHP 100,000',
   },
  { id: 4, username: 'Jon Snow', avatar: 'https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  email: 'jon@gmail.com',
  status: 'Active',
  transaction: 'PHP 100,000',
   },
  { id: 5, username: 'Jon Snow', avatar: 'https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  email: 'jon@gmail.com',
  status: 'Active',
  transaction: 'PHP 100,000',
   },
  { id: 6, username: 'Jon Snow', avatar: 'https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  email: 'jon@gmail.com',
  status: 'Active',
  transaction: 'PHP 100,000',
   },
  { id: 7, username: 'Jon Snow', avatar: 'https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  email: 'jon@gmail.com',
  status: 'Active',
  transaction: 'PHP 100,000',
   },
  { id: 8, username: 'Jon Snow', avatar: 'https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  email: 'jon@gmail.com',
  status: 'Active',
  transaction: 'PHP 100,000',
   },
  { id: 9, username: 'Jon Snow', avatar: 'https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  email: 'jon@gmail.com',
  status: 'Active',
  transaction: 'PHP 100,000',
   },
  { id: 10, username: 'Jon Snow', avatar: 'https://images.pexels.com/photos/3907595/pexels-photo-3907595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  email: 'jon@gmail.com',
  status: 'Active',
  transaction: 'PHP 100,000',
   },
  
];



export const productRows = [

  { id: 1, name: 'Apple Ipod', img: 'https://images.pexels.com/photos/164827/pexels-photo-164827.jpeg?auto=compress&cs=tinysrgb&w=1600',
  stock: 123,
  status: 'Active',
  price: 'PHP 12000',
   },
  { id: 2, name: 'Apple Ipods', img: 'https://images.pexels.com/photos/164827/pexels-photo-164827.jpeg?auto=compress&cs=tinysrgb&w=1600',
  stock: 123,
  status: 'Active',
  price: 'PHP 12000',
   },
  { id: 3, name: 'Apple Ipods', img: 'https://images.pexels.com/photos/164827/pexels-photo-164827.jpeg?auto=compress&cs=tinysrgb&w=1600',
  stock: 123,
  status: 'Active',
  price: 'PHP 12000',
   },
  { id: 4, name: 'Apple Ipods', img: 'https://images.pexels.com/photos/164827/pexels-photo-164827.jpeg?auto=compress&cs=tinysrgb&w=1600',
  stock: 123,
  status: 'Active',
  price: 'PHP 12000',
   },
  { id: 5, name: 'Apple Ipods', img: 'https://images.pexels.com/photos/164827/pexels-photo-164827.jpeg?auto=compress&cs=tinysrgb&w=1600',
  stock: 123,
  status: 'Active',
  price: 'PHP 12000',
   },
  { id: 6, name: 'Apple Ipods', img: 'https://images.pexels.com/photos/164827/pexels-photo-164827.jpeg?auto=compress&cs=tinysrgb&w=1600',
  stock: 123,
  status: 'Active',
  price: 'PHP 12000',
   },
  { id: 7, name: 'Apple Ipods', img: 'https://images.pexels.com/photos/164827/pexels-photo-164827.jpeg?auto=compress&cs=tinysrgb&w=1600',
  stock: 123,
  status: 'Active',
  price: 'PHP 12000',
   },
  { id: 8, name: 'Apple Ipods', img: 'https://images.pexels.com/photos/164827/pexels-photo-164827.jpeg?auto=compress&cs=tinysrgb&w=1600',
  stock: 123,
  status: 'Active',
  price: 'PHP 12000',
   },
  { id: 9, name: 'Apple Ipods', img: 'https://images.pexels.com/photos/164827/pexels-photo-164827.jpeg?auto=compress&cs=tinysrgb&w=1600',
  stock: 123,
  status: 'Active',
  price: 'PHP 12000',
   },
  { id: 10, name: 'Apple Ipods', img: 'https://images.pexels.com/photos/164827/pexels-photo-164827.jpeg?auto=compress&cs=tinysrgb&w=1600',
  stock: 123,
  status: 'Active',
  price: 'PHP 12000',
   },
   
  
];
